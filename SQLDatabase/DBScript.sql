USE [master]
GO
/****** Object:  Database [BrowserTravelSolutionsDB]    Script Date: 20/06/2022 7:45:08 p. m. ******/
CREATE DATABASE [BrowserTravelSolutionsDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BrowserTravelSolutionsDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\BrowserTravelSolutionsDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BrowserTravelSolutionsDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\BrowserTravelSolutionsDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BrowserTravelSolutionsDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET RECOVERY FULL 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET  MULTI_USER 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'BrowserTravelSolutionsDB', N'ON'
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET QUERY_STORE = OFF
GO
USE [BrowserTravelSolutionsDB]
GO
/****** Object:  Table [dbo].[autores]    Script Date: 20/06/2022 7:45:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[autores](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](45) NOT NULL,
	[apellidos] [varchar](45) NOT NULL,
 CONSTRAINT [PK_autores] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[autores_has_libros]    Script Date: 20/06/2022 7:45:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[autores_has_libros](
	[autores_id] [int] NOT NULL,
	[libros_ISBN] [bigint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[editoriales]    Script Date: 20/06/2022 7:45:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[editoriales](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](45) NOT NULL,
	[sede] [varchar](45) NOT NULL,
 CONSTRAINT [PK_editoriales] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[libros]    Script Date: 20/06/2022 7:45:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[libros](
	[ISBN] [bigint] IDENTITY(1,1) NOT NULL,
	[editoriales_id] [int] NOT NULL,
	[titulo] [varchar](45) NOT NULL,
	[sinopsis] [text] NOT NULL,
	[n_paginas] [varchar](45) NOT NULL,
 CONSTRAINT [PK_libros] PRIMARY KEY CLUSTERED 
(
	[ISBN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[autores_has_libros]  WITH CHECK ADD  CONSTRAINT [FK_autores_has_libros_autores] FOREIGN KEY([autores_id])
REFERENCES [dbo].[autores] ([id])
GO
ALTER TABLE [dbo].[autores_has_libros] CHECK CONSTRAINT [FK_autores_has_libros_autores]
GO
ALTER TABLE [dbo].[autores_has_libros]  WITH CHECK ADD  CONSTRAINT [FK_autores_has_libros_libros] FOREIGN KEY([libros_ISBN])
REFERENCES [dbo].[libros] ([ISBN])
GO
ALTER TABLE [dbo].[autores_has_libros] CHECK CONSTRAINT [FK_autores_has_libros_libros]
GO
ALTER TABLE [dbo].[libros]  WITH CHECK ADD  CONSTRAINT [FK_libros_editoriales] FOREIGN KEY([editoriales_id])
REFERENCES [dbo].[editoriales] ([id])
GO
ALTER TABLE [dbo].[libros] CHECK CONSTRAINT [FK_libros_editoriales]
GO
USE [master]
GO
ALTER DATABASE [BrowserTravelSolutionsDB] SET  READ_WRITE 
GO
