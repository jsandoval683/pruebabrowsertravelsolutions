﻿using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrowserTravelSolutionsWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutoresHasLibrosController : ControllerBase
    {
        private readonly AutoresHasLibroService _autoresHasLibroService;

        public AutoresHasLibrosController(AutoresHasLibroService autoresHasLibroService)
        {
            _autoresHasLibroService = autoresHasLibroService;
        }

        [HttpPost("AddAutoresHasLibro")]
        public async Task<IActionResult> AddAutoresHasLibro(AutoresHasLibro autoresHasLibro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await _autoresHasLibroService.AddAutoresHasLibro(autoresHasLibro);

                if (response != null)
                {
                    return new OkObjectResult(response);
                }
                else
                {
                    return BadRequest("No se encontraron resultados");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("GetAutoresHasLibros")]
        public async Task<IActionResult> GetAutoresHasLibros()
        {
            try
            {
                var result = await _autoresHasLibroService.GetAutoresHasLibros();
                if (result != null && result.Count != 0)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("No se encontraron resultados");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("GetAutoresHasLibroByIdAutor/{id}")]
        public async Task<IActionResult> GetAutoresHasLibroByIdAutor(int id)
        {
            try
            {
                var result = await _autoresHasLibroService.GetAutoresHasLibroByIdAutor(id);
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("No se encontraron resultados");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("GetAutoresHasLibroByIdLibro/{id}")]
        public async Task<IActionResult> GetAutoresHasLibroByIdLibro(int id)
        {
            try
            {
                var result = await _autoresHasLibroService.GetAutoresHasLibroByIdLibro(id);
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("No se encontraron resultados");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("UpdateAutoresHasLibro")]
        public async Task<IActionResult> UpdateAutoresHasLibro(AutoresHasLibro autoresHasLibro)
        {
            try
            {
                var result = await _autoresHasLibroService.UpdateAutoresHasLibro(autoresHasLibro);
                if (result)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("No se realizaron cambios a ningún dato existente");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("DeleteAutoresHasLibro/{id}")]
        public async Task<IActionResult> DeleteAutoresHasLibro(int id)
        {
            try
            {
                var result = await _autoresHasLibroService.DeleteAutoresHasLibro(id);
                if (result)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("No se eliminó ningun registro existente");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
