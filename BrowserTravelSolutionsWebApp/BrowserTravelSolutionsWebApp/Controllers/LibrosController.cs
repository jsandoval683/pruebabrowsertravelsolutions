﻿using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrowserTravelSolutionsWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibrosController : Controller
    {
        private readonly LibroService _libroService;

        public LibrosController(LibroService libroService)
        {
            _libroService = libroService;
        }

        [HttpPost("AddLibro")]
        public async Task<IActionResult> AddLibro(Libro libro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await _libroService.AddLibro(libro);

                if (response != null)
                {
                    return new OkObjectResult(response);
                }
                else
                {
                    return BadRequest("No se encontraron resultados");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("GetLibros")]
        public async Task<IActionResult> GetLibros()
        {
            try
            {
                var result = await _libroService.GetLibros();
                if (result != null && result.Count != 0)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("No se encontraron resultados");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("GetLibroById/{id}")]
        public async Task<IActionResult> GetLibroById(int id)
        {
            try
            {
                var result = await _libroService.GetLibroById(id);
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("No se encontraron resultados");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("UpdateLibro")]
        public async Task<IActionResult> UpdateLibro(Libro libro)
        {
            try
            {
                var result = await _libroService.UpdateLibro(libro);
                if (result)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("No se realizaron cambios a ningún dato existente");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("DeleteLibro/{id}")]
        public async Task<IActionResult> DeleteLibro(int id)
        {
            try
            {
                var result = await _libroService.DeleteLibro(id);
                if (result)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("No se eliminó ningun registro existente");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
