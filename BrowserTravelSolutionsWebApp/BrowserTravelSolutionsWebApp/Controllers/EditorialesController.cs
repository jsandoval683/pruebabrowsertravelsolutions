﻿using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrowserTravelSolutionsWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EditorialesController : ControllerBase
    {
        private readonly EditorialesService _editorialesService;

        public EditorialesController(EditorialesService editorialesService)
        {
            _editorialesService = editorialesService;
        }

        [HttpPost("AddEditorial")]
        public async Task<IActionResult> AddEditorial(Editoriale editoriale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await _editorialesService.AddEditorial(editoriale);

                if (response != null)
                {
                    return new OkObjectResult(response);
                }
                else
                {
                    return BadRequest("No se encontraron resultados");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("GetEditoriales")]
        public async Task<IActionResult> GetEditoriales()
        {
            try
            {
                var result = await _editorialesService.GetEditoriales();
                if (result != null && result.Count != 0)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("No se encontraron resultados");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("GetEditorialById/{id}")]
        public async Task<IActionResult> GetEditorialById(int id)
        {
            try
            {
                var result = await _editorialesService.GetEditorialById(id);
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("No se encontraron resultados");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("UpdateEditorial")]
        public async Task<IActionResult> UpdateEditorial(Editoriale editoriale)
        {
            try
            {
                var result = await _editorialesService.UpdateEditorial(editoriale);
                if (result)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("No se realizaron cambios a ningún dato existente");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("DeleteEditorial/{id}")]
        public async Task<IActionResult> DeleteEditorial(int id)
        {
            try
            {
                var result = await _editorialesService.DeleteEditorial(id);
                if (result)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("No se eliminó ningun registro existente");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
