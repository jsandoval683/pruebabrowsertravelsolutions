﻿using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrowserTravelSolutionsWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutoresController : ControllerBase
    {
        private readonly AutoresService _autoresService;

        public AutoresController(AutoresService autoresService)
        {
            _autoresService = autoresService;
        }

        [HttpPost("AddAutor")]
        public async Task<IActionResult> AddAutor(Autore autore)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await _autoresService.AddAutor(autore);

                if (response != null)
                {
                    return new OkObjectResult(response);
                }
                else
                {
                    return BadRequest("No se encontraron resultados");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("GetAutores")]
        public async Task<IActionResult> GetAutores()
        {
            try
            {
                var result = await _autoresService.GetAutores();
                if (result != null && result.Count != 0)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("No se encontraron resultados");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("GetAutorById/{id}")]
        public async Task<IActionResult> GetAutorById(int id)
        {
            try
            {
                var result = await _autoresService.GetAutorById(id);
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("No se encontraron resultados");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("UpdateAutor")]
        public async Task<IActionResult> UpdateAutor(Autore autore)
        {
            try
            {
                var result = await _autoresService.UpdateAutor(autore);
                if (result)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("No se realizaron cambios a ningún dato existente");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("DeleteAutor/{id}")]
        public async Task<IActionResult> DeleteAutor(int id)
        {
            try
            {
                var result = await _autoresService.DeleteAutor(id);
                if (result)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("No se eliminó ningun registro existente");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
