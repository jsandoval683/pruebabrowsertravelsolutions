﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class AutoresService
    {
        private readonly BrowserTravelSolutionsDBContext _DBcontext;

        public AutoresService(BrowserTravelSolutionsDBContext DBcontext)
        {
            _DBcontext = DBcontext;
        }

        public async Task<Autore> AddAutor(Autore autore)
        {
            try
            {
                await _DBcontext.AddAsync(autore);
                await _DBcontext.SaveChangesAsync();
                return autore;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<List<Autore>> GetAutores()
        {
            try
            {
                return await _DBcontext.Autores.ToListAsync();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<Autore> GetAutorById(int id)
        {
            try
            {
                return await _DBcontext.Autores.FirstOrDefaultAsync(x => x.Id == id);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<bool> UpdateAutor(Autore autore)
        {
            try
            {
                _DBcontext.Autores.Update(autore);
                await _DBcontext.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<bool> DeleteAutor(int id)
        {
            try
            {
                Autore autore = await _DBcontext.Autores.FirstOrDefaultAsync(x => x.Id == id);
                _DBcontext.Autores.Remove(autore);
                await _DBcontext.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
