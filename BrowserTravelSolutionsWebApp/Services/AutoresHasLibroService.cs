﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class AutoresHasLibroService
    {
        private readonly BrowserTravelSolutionsDBContext _DBcontext;

        public AutoresHasLibroService(BrowserTravelSolutionsDBContext DBcontext)
        {
            _DBcontext = DBcontext;
        }

        public async Task<AutoresHasLibro> AddAutoresHasLibro(AutoresHasLibro autoresHasLibro)
        {
            try
            {
                await _DBcontext.AddAsync(autoresHasLibro);
                await _DBcontext.SaveChangesAsync();
                return autoresHasLibro;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<List<AutoresHasLibro>> GetAutoresHasLibros()
        {
            try
            {
                return await _DBcontext.AutoresHasLibros.ToListAsync();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<AutoresHasLibro> GetAutoresHasLibroByIdAutor(int id)
        {
            try
            {
                return await _DBcontext.AutoresHasLibros.FirstOrDefaultAsync(x => x.AutoresId == id);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<AutoresHasLibro> GetAutoresHasLibroByIdLibro(int id)
        {
            try
            {
                return await _DBcontext.AutoresHasLibros.FirstOrDefaultAsync(x => x.LibrosIsbn == id);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<bool> UpdateAutoresHasLibro(AutoresHasLibro autoresHasLibro)
        {
            try
            {
                _DBcontext.AutoresHasLibros.Update(autoresHasLibro);
                await _DBcontext.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<bool> DeleteAutoresHasLibro(int id)
        {
            try
            {
                AutoresHasLibro autoresHasLibro = await _DBcontext.AutoresHasLibros.FirstOrDefaultAsync(x => x.AutoresId == id);
                _DBcontext.AutoresHasLibros.Remove(autoresHasLibro);
                await _DBcontext.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
