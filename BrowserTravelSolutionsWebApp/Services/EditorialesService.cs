﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class EditorialesService
    {
        private readonly BrowserTravelSolutionsDBContext _DBcontext;

        public EditorialesService(BrowserTravelSolutionsDBContext DBcontext)
        {
            _DBcontext = DBcontext;
        }

        public async Task<Editoriale> AddEditorial(Editoriale editoriale)
        {
            try
            {
                await _DBcontext.AddAsync(editoriale);
                await _DBcontext.SaveChangesAsync();
                return editoriale;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<List<Editoriale>> GetEditoriales()
        {
            try
            {
                return await _DBcontext.Editoriales.ToListAsync();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<Editoriale> GetEditorialById(int id)
        {
            try
            {
                return await _DBcontext.Editoriales.FirstOrDefaultAsync(x => x.Id == id);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<bool> UpdateEditorial(Editoriale editoriale)
        {
            try
            {
                _DBcontext.Editoriales.Update(editoriale);
                await _DBcontext.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<bool> DeleteEditorial(int id)
        {
            try
            {
                Editoriale editoriale = await _DBcontext.Editoriales.FirstOrDefaultAsync(x => x.Id == id);
                _DBcontext.Editoriales.Remove(editoriale);
                await _DBcontext.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
