﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class LibroService
    {
        private readonly BrowserTravelSolutionsDBContext _DBcontext;

        public LibroService(BrowserTravelSolutionsDBContext DBcontext)
        {
            _DBcontext = DBcontext;
        }

        public async Task<Libro> AddLibro(Libro libro)
        {
            try
            {
                await _DBcontext.AddAsync(libro);
                await _DBcontext.SaveChangesAsync();
                return libro;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<List<Libro>> GetLibros()
        {
            try
            {
                return await _DBcontext.Libros.ToListAsync();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<Libro> GetLibroById(int id)
        {
            try
            {
                return await _DBcontext.Libros.FirstOrDefaultAsync(x => x.Isbn == id);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<bool> UpdateLibro(Libro libro)
        {
            try
            {
                _DBcontext.Libros.Update(libro);
                await _DBcontext.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<bool> DeleteLibro(int id)
        {
            try
            {
                Libro libro = await _DBcontext.Libros.FirstOrDefaultAsync(x => x.Isbn == id);
                _DBcontext.Libros.Remove(libro);
                await _DBcontext.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
